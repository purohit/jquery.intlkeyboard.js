# jquery.intlKeyboard.js

A jQuery plugin that lets people with English/Latin keyboards easily
type in other languages by replacing keystrokes on an HTML element.

For Latin-extended languages like `fra, esp, deu, tur`, usage is simple:

    $("input.learn-french").intlKeyboard({
        language: "fra"
    });

In the element, a user can now type combinations like `` ` `` + `e`, and
a `è` will appear in its place. It is up to the developer to tell the
mappings to the user. Check out this Pinyin example with custom CSS:

![Pinyin example](http://i.imgur.com/ZLuYOoM.png)

For IME languages like `zho`, the plugin asks
Google Input Tools for a suggestion based on the user's typing, and shows `numResults`
numbered suggestions in the `$(options.suggestions)` you have passed in. When the user
types a number, clicks a suggestion, or hits `[Space]/[Enter]` (
which picks the first suggestion), the input is replaced
and the suggestions box is hidden. Style the suggestions box as you like.

    $("input.learn-mandarin").intlKeyboard({
        language: "zho",
        suggestions: "#suggestionsBox",
        numResults: 5 // default
    });

This plugin is intended to aid language-learners who don't want to install a keyboard.
Serious typists of foreign languages should install the actual keyboard software.

## Supported languages
French, Spanish, German, Turkish, and Chinese Pinyin. Pull requests with new keyboard mappings
for a language are welcomed and appreciated! There is not yet support for
languages with their own non-Latin, non-ideographic alphabets, such as
Arabic and Greek. This is the last "class" of orthography to support,
and I fully intend to when I have time. Google Input Tools also provides
suggestions for this class of languages, so it might be as simple as adding the
language codes in.

## Contributing
If you are creating a new set of keyboard mappings, try to follow precedents
set by other languages. For example, in both French and Turkish, the letter
`ç` is used, and the mapping is `,` + `c` in both cases.

Good mappings are intuitive and functional. For
example, to put a circonflexe on an `e`, the mapping
`^` + `e` = `ê` is intuitive because users can already type the
caret easily, and because `^` looks like the hat on the `e`. It is functional because there is hardly a use for typing
`^e` in succession (outside of mathematics), so we can use it as a mapping.

Sometimes, there are unavoidable collisions. For example, in French there is
a character `é` that is intuitively mapped to `'` + `e`. However, it
is common in French to want to write words like `célebrer` and `c'est`. Because
of this conflict, a user can purposely type "c'e" by typing `c` + `'`
+ `[space]` + `e`.

### Minification
The plugin uses uglifyjs2 for minification, and to beautify the source:

    // Beautify source
    uglifyjs jquery.intlKeyboard.js --beautify --comments=all --lint

    // Compress
    uglifyjs jquery.intlKeyboard.js --compress > jquery.intlKeyboard.min.js


